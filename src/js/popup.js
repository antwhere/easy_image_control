﻿var btnArray = ['image', 'title', 'favicon', 'exclude', 'enable'];
var Popup = {
    main: function () {
        btnArray.forEach(function (btn) {
            if (btn == 'exclude') {
                document.getElementById(btn).addEventListener("click", Popup.excludeListClick); // 绑定列表事件
                return;
            } else if (btn == 'enable') { // enable排除状态
                Popup.enableStatus(btn);
                document.getElementById(btn).addEventListener("click", function (event) { // 绑定点击事件
                    var targetEle = event.target || event.srcElement;
                    if (targetEle.nodeName.toLowerCase() == 'input') Popup.enableClick(targetEle, targetEle.checked);
                });
            } else {
                Popup.btnStatus(btn);
                document.getElementById(btn).addEventListener("click", function (event) { // 绑定点击事件
                    var targetEle = event.target || event.srcElement;
                    if (targetEle.nodeName.toLowerCase() == 'input') Popup.btnClick(targetEle, targetEle.checked);
                });
            }
        });
        document.querySelector('body').style.display = 'block';
    },
    btnStatus: function (btn) {
        dataGet(btn, function (data) {
            log('button: ' + btn, data);
            var flag = true;
            if (data[btn] != undefined) {
                flag = data[btn];
            }
            document.getElementById(btn).querySelector('input').checked = flag;
        });
    },
    btnClick: function (targetEle, checked) {
        var inpType = targetEle.getAttribute('inp');
        var setVal = new Object();
        setVal[inpType] = checked;
        log('btn click', inpType, checked, setVal);
        dataSet(setVal, function () {
            if (inpType == 'image') { // 更新chrome菜单的设置
                sendMsg({
                    msg: 'updateChromeImageConfig'
                });
            }
        });
    },
    enableStatus: function (btn) {
        getNowTab(function (tab) {
            var url = getDomainFromUrl(tab[0].url);
            var flag = true;
            sendMsg({
                msg: 'enableStatus',
                url: url
            }, function (response) {
                if (!response.msg) {
                    flag = response.msg;
                }
                document.getElementById(btn).querySelector('input').checked = flag;
            });
        });
    },
    enableClick: function (targetEle, checked) {
        var inpType = targetEle.getAttribute('inp');
        log('enable click', inpType, checked);
        getNowTab(function (tab) {
            var url = getDomainFromUrl(tab[0].url);
            dataGet('enable', function (data) {
                if (data.enable != undefined) {
                    var newEnable = [];
                    data.enable.forEach(function (obj, i) {
                        if (obj == url && !checked) { // 删除已有规则
                        } else {
                            newEnable.push(obj);
                        }
                    });
                    if (checked) {
                        newEnable.push(url);
                    }
                    dataSet({
                        enable: newEnable
                    }, function () { // 更新chrome菜单的设置
                        sendMsg({
                            msg: 'updateChromeImageConfig'
                        });
                    });
                }
            });

        });
    },
    excludeListClick: function () {
        log('exclude click');
        openURL(chrome.runtime.getURL('exclude.html'));
    }
};

Popup.main(); // main