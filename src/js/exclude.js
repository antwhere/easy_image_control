﻿dataGet('enable', function (data) {
    enable = data.enable;
    var tbody = document.querySelector('#tbody');
    var str = '';
    enable.forEach(function (obj, i) {
        str += '<tr><td>' + (i + 1) + '</td><td>' + obj + '</td><td><button url="' + obj + '" type="button" class="_js_del btn btn-danger btn-xs">remove</button></td></tr>';
    });
    tbody.innerHTML = str;

    var delBtn = document.querySelectorAll('._js_del');
    for (var i = 0; i < delBtn.length; i++) {
        delBtn[i].addEventListener('click', function (event) {
            var targetEle = event.target || event.srcElement;
            var url = targetEle.getAttribute('url');
            dataGet('enable', function (data) {
                if (data.enable) {
                    var newEnable = [];
                    data.enable.forEach(function (obj, i) {
                        if (obj != url) {
                            newEnable.push(obj);
                        }
                    });
                    dataSet({
                        enable: newEnable
                    }, function () { // 更新chrome菜单的设置
                        sendMsg({
                            msg: 'updateChromeImageConfig'
                        }, function (msg) {
                            window.location.reload();
                        });
                    });
                }
            });
        });
    }
});

dataGet('notclean', function (data) {
    var notcleanInp = document.getElementById('notclean');
    if (data && isNotEmpty(data.notclean) && data.notclean) {
        notcleanInp.checked = true;
    }
    notcleanInp.addEventListener('click', function () {
        dataSet({
            notclean: notcleanInp.checked
        }, function () {
            window.location.reload();
        });
    });
});

(function () { // Import/Export Settings
    var exportBtn = document.getElementById('export');
    var importBtn = document.getElementById('import');
    exportBtn.addEventListener('click', function () {
        var exportObj = {
            image: true,
            title: true,
            favicon: true,
            notclean: false,
            enable: []
        };
        dataGet('image', function (data) {
            if (data.image != undefined) {
                exportObj.image = data.image;
            }
            dataGet('title', function (data) {
                if (data.title != undefined) {
                    exportObj.title = data.title;
                }
                dataGet('favicon', function (data) {
                    if (data.favicon != undefined) {
                        exportObj.favicon = data.favicon;
                    }
                    dataGet('notclean', function (data) {
                        if (data.notclean != undefined) {
                            exportObj.notclean = data.notclean;
                        }
                        dataGet('enable', function (data) {
                            if (data.enable != undefined) {
                                exportObj.enable = data.enable;
                            }
                            downloadFile('Easy_Image_Control-' + formatDate(new Date(), 'yyyy_MM_dd_HH_mm_ss') + '.json', JSON.stringify(exportObj));
                        });
                    });
                });
            });
        });
    });
    importBtn.addEventListener('click', function () {
        var fileInput = document.getElementById('fileInput');
        fileInput.addEventListener('change', function (e) {
            var file = fileInput.files[0];
            var reader = new FileReader();
            reader.onload = function (e) {
                var content = reader.result;
                var obj = JSON.parse(content);
                dataSet({
                    image: obj.image
                }, function () {
                    dataSet({
                        title: obj.title
                    }, function () {
                        dataSet({
                            favicon: obj.favicon
                        }, function () {
                            dataSet({
                                notclean: obj.notclean
                            }, function () {
                                dataSet({
                                    enable: obj.enable
                                }, function () {
                                    // 更新chrome菜单的设置
                                    sendMsg({
                                        msg: 'updateChromeImageConfig'
                                    });
                                    alert('Import successful!');
                                    window.location.reload();
                                });
                            });
                        });
                    });
                });
            }
            reader.readAsText(file);
        });
        fileInput.click();
    });
})();