﻿// ----- 配置参数 -----
var config = {
    log: false
};
var faviconBase64 = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAIUlEQVQ4EWP8//8/AyWAiRLNIL2jBoyGwWg6gOSigc8LAGOaAx0s99CyAAAAAElFTkSuQmCC';
var titleReplace = 'Google Chrome';

// ----- 全局变量 -----
var allTabIdAndUrl = {}; // 所有标签页id及url

// ----- 公用方法 -----
/**打开新窗口*/
var openURL = function (url) {
    window.open(url);
};

/**获取域名*/
function getDomainFromUrl(url) {
    var host = "null";
    if (typeof url == "undefined" || null == url) {
        url = window.location.href;
    }
    var regex = /.*\:\/\/([^\/]*).*/;
    var match = url.match(regex);
    if (typeof match != "undefined" && null != match) {
        host = match[1].replace(/.*@/, '');
    }
    return host;
}

/**判断域名是否在url列表*/
function existList(url, urlList) {
    var returnValue = false;
    for (var i = 0; isNotEmpty(urlList) && i < urlList.length; i++) {
        if (url == urlList[i]) {
            returnValue = true;
            break;
        }
    }
    return returnValue;
}

/**判断域名是否在url Map*/
function existMap(url, urlMap) {
    return url in urlMap;
}

/**判断字符串中是否存在指定后缀*/
function isExistPostfix(str, postfix) {
    if (str) {
        return str.toLowerCase().lastIndexOf(postfix) == (str.length - postfix.length) ? true : false;
    } else {
        return false;
    }
}

/**log输出*/
function log() {
    if (!config.log) return;
    if (arguments) {
        for (var i = 0; i < arguments.length; i++) {
            console.log(arguments[i]);
        }
    }
}

/**向background发送消息*/
function sendMsg(msg, callback) { // msg例子：{msg: "faviconStatus"}
    chrome.runtime.sendMessage(msg, callback);
}

/**数据读取、保存、删除*/
function dataGet(key, callFn) { // callFn回调获取data
    chrome.storage.local.get(key, function (data) {
        callFn(data);
    });
}

function dataSet(obj, callFn) { // callFn可选
    // chrome.storage.sync.set({'key': 'value'}, callFn);// chrome.storage.sync 调用次数过多会有
    // “This request exceeds the MAX_WRITE_OPERATIONS_PER_HOUR quota” 这个超过最大单位小时限制，故
    // 在此使用“chrome.storage.local”
    chrome.storage.local.set(obj, callFn);
}

function dataRemove(keys, callFn) { // keys:A single key or a list of keys for items to remove. ; callFn可选
    chrome.storage.local.remove(key, callFn);
}

function dataClear(callFn) { // callFn可选
    chrome.storage.local.clear(callFn);
}

/**判断是否为空*/
function isNotEmpty(obj) {
    return typeof obj != 'undefined' && obj != null ? true : false;
}

/**获取tabs信息*/
function getTab(tabId, callFn) {
    if (!isNotEmpty(tabId) || !isNotEmpty(callFn)) return new Object();
    chrome.tabs.get(tabId, callFn);
}

/**获取当前选中的标签页*/
function getNowTab(callFn) {
    chrome.tabs.query({
        active: true,
        currentWindow: true
    }, callFn);
}

/**获取所有标签页*/
function getAllTab(callFn) {
    chrome.tabs.query({}, callFn);
}

/**更新所有标签页id及url*/
function updateAllTabIdAndUrl() {
    getAllTab(function (tabs) {
        var newTabIdAndUrl = {};
        for (var i = 0; i < tabs.length; i++) {
            var url = tabs[i].url;
            if (isNotEmpty(url) && url.length > 0) {

            } else if (tabs[i].pendingUrl) {
                url = tabs[i].pendingUrl;
            }
            newTabIdAndUrl[tabs[i].id] = getDomainFromUrl(url);
        }
        allTabIdAndUrl = newTabIdAndUrl;
    });
}

/**清除浏览器缓存*/
function cleanBrowsingData(callFn) {
    log('cleanBrowsingData...');
    chrome.browsingData.remove({ // 删除缓存
        'since': 0,
        'originTypes': {
            'unprotectedWeb': true,
            'protectedWeb': true,
            'extension': true
        }
    }, {
        'cache': true
    }, callFn);
}

/**
 * 格式化日期
 * 例子：formatDate(new Date(), 'yyyy-MM-dd HH:mm:ss');
 */
function formatDate(date, formatStr) {
    var str = formatStr;
    var Week = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

    str = str.replace(/yyyy|YYYY/, date.getFullYear());
    str = str.replace(/yy|YY/, (date.getYear() % 100) > 9 ? (date.getYear() % 100).toString() : '0' + (date.getYear() % 100));

    date.setMonth(date.getMonth() + 1);
    str = str.replace(/MM/, date.getMonth() > 9 ? date.getMonth().toString() : '0' + date.getMonth());
    str = str.replace(/M/g, date.getMonth());

    str = str.replace(/w|W/g, Week[date.getDay()]);

    str = str.replace(/dd|DD/, date.getDate() > 9 ? date.getDate().toString() : '0' + date.getDate());
    str = str.replace(/d|D/g, date.getDate());

    str = str.replace(/hh|HH/, date.getHours() > 9 ? date.getHours().toString() : '0' + date.getHours());
    str = str.replace(/h|H/g, date.getHours());
    str = str.replace(/mm/, date.getMinutes() > 9 ? date.getMinutes().toString() : '0' + date.getMinutes());
    str = str.replace(/m/g, date.getMinutes());

    str = str.replace(/ss|SS/, date.getSeconds() > 9 ? date.getSeconds().toString() : '0' + date.getSeconds());
    str = str.replace(/s|S/g, date.getSeconds());

    return str;
};

/**下载文件*/
function downloadFile(fileName, content) {
    var aLink = document.createElement('a');
    var blob = new Blob([content]);
    var evt = document.createEvent("HTMLEvents");
    evt.initEvent("click", false, false);
    aLink.download = fileName;
    aLink.href = URL.createObjectURL(blob);
    aLink.dispatchEvent(evt);
    aLink.click();
}

/**Array转Map*/
function arrayToMap(arr) {
    var map = {};
    if (isNotEmpty(arr)) {
        for (var i = 0; i < arr.length; i++) {
            map[arr[i]] = true;
        }
    }
    return map;
}

/**startsWith*/
function startsWith(str, match) {
    return str.lastIndexOf(match, 0) === 0;
}