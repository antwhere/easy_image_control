/**插入icon*/
function insertIcon(icon) {
    beforeLoadHandler(); // 先删除icon
    var head = document.getElementsByTagName("head")[0];
    if (head) {
        var link = document.createElement("link");
        link.href = icon;
        link.type = "image/png";
        link.rel = "shortcut icon";
        head.appendChild(link);
    }
}

/**删除icon*/
function beforeLoadHandler() {
    var links = document.getElementsByTagName('link');
    var removeArray = [];
    for (var i = 0; i < links.length; i++) {
        if (links[i].getAttribute('rel') &&
            (links[i].getAttribute('rel') == 'shortcut icon' || links[i].getAttribute('rel') == 'icon') &&
            (links[i].getAttribute('href') && links[i].getAttribute('href') != faviconBase64) // 排除新插入的icon
        ) {
            removeArray.push(links[i]);
        }
    }
    for (var i = 0; i < removeArray.length; i++) {
        removeArray[i].remove();
    }
}

/**处理title*/
function replaceTitle(str) {
    log('replaceTitle');
    var title = document.getElementsByTagName('title');
    if (title && title.length > 0) {
        log('title length: ' + title.length);
        var newTitle = title[0].cloneNode(true);
        newTitle.text = str;
        title[0].parentNode.replaceChild(newTitle, title[0]);
    }
    if (document.title != str) {
        document.title = str;
    }
    titleBind();
}

function titleBind() {
    log('titleBind');
    var title = document.getElementsByTagName('title');
    if (title && title.length > 0) {
        log('title length: ' + title.length);
        title[0].addEventListener('DOMSubtreeModified', function (e) {
            replaceTitle(titleReplace);
        }, true);
    }
}


/**处理image*/
function replaceBacImg() {}

/**main入口*/
function main() {
    var url = getDomainFromUrl(window.location.href);
    sendMsg({
        msg: 'faviconStatus',
        url: url
    }, function (response) {
        if (!response.msg) {
            insertIcon(faviconBase64);
        }
    });
    sendMsg({
        msg: 'titleStatus',
        url: url
    }, function (response) {
        if (!response.msg) {
            replaceTitle(titleReplace);
        }
    });
    sendMsg({
        msg: 'imageStatus',
        url: url
    }, function (response) {
        if (!response.msg) {
            replaceBacImg();
        }
    });
}

main();