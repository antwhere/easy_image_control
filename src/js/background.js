﻿// 以下变量做缓存供网络请求判断使用
var image = true;
var title = true;
var favicon = true;
var notclean = false;
var enable = [];
var enableMap = arrayToMap(enable);

var retry = 60;
var cache = {
    ttl: 30 * 1000,
    data: new Map(),
    lastUpdateTime: new Date().getTime(),
    set: function (url) {
        if ((new Date().getTime() - cache.lastUpdateTime) > cache.ttl ||
            cache.data.size > 10000) {
            cache.data.clear();
        }
        cache.lastUpdateTime = new Date().getTime();
        var count = cache.get(url);
        if (cache.get(url) != -1) {
            cache.data.set(url, count + 1);
        } else {
            cache.data.set(url, 1);
        }
    },
    get: function (url) {
        if (cache.data.has(url)) {
            return cache.data.get(url);
        }
        return -1;
    }
};

/**初始化*/
function init() {
    log('background init');
    dataGet('image', function (data) {
        if (data.image == undefined || typeof (data.image) != 'boolean') {
            dataSet({
                image: image
            });
        } else {
            image = data.image;
        }
    });
    dataGet('title', function (data) {
        if (data.title == undefined || typeof (data.title) != 'boolean') {
            dataSet({
                title: title
            });
        } else {
            title = data.title;
        }
    });
    dataGet('favicon', function (data) {
        if (data.favicon == undefined || typeof (data.favicon) != 'boolean') {
            dataSet({
                favicon: favicon
            });
        } else {
            favicon = data.favicon;
        }
    });
    dataGet('notclean', function (data) {
        if (data.notclean == undefined || typeof (data.notclean) != 'boolean') {
            dataSet({
                notclean: notclean
            });
        } else {
            notclean = data.notclean;
        }
    });
    dataGet('enable', function (data) {
        if (data.enable == undefined || typeof (data.enable) != 'object') {
            dataSet({
                enable: enable
            }, function () {});
        } else {
            enable = data.enable;
            enableMap = arrayToMap(enable);
        }
    });
}

/**更新开关图标*/
function updateBadge() {
    chrome.tabs.query({
        active: true
    }, function (tab) {
        if (tab) {
            tab.forEach(function (t) {
                var str = ''; // If an empty string ('') is passed, the badge text is cleared.
                var url = getDomainFromUrl(t.url);
                if (!existMap(url, enableMap)) {
                    chrome.browserAction.setBadgeBackgroundColor({
                        color: '#EE3131',
                        tabId: t.id
                    });
                    if (!image)
                        str += 'm';
                    if (!title)
                        str += 't';
                    if (!favicon)
                        str += 'f';
                }
                chrome.browserAction.setBadgeText({
                    text: str,
                    tabId: t.id
                });
            });
        }
    });
}

/**更新映射关系*/
function badgeAndUrlUpdate() {
    updateAllTabIdAndUrl();
    updateBadge();
}

// ----- main方法 -----
function main() {
    init(); // 初始化

    // 扩展重新安装或更新新版本触发
    chrome.runtime.onInstalled.addListener(function () {
        log('onInstalled');
    });

    // 设置listener并更新标签页id与url对应关系
    chrome.tabs.onRemoved.addListener(badgeAndUrlUpdate);
    chrome.tabs.onCreated.addListener(badgeAndUrlUpdate);
    chrome.tabs.onUpdated.addListener(badgeAndUrlUpdate);
    chrome.tabs.onActivated.addListener(badgeAndUrlUpdate);
    chrome.tabs.onHighlighted.addListener(badgeAndUrlUpdate);

    // 消息监听
    chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
        if (request.msg == 'updateChromeImageConfig') { // 添加或移除exclude条目后更新chrome菜单的设置
            if (!notclean) { // 清除浏览器缓存
                cleanBrowsingData();
            }
            sendResponse({
                msg: true
            });
        } else if (request.msg == 'imageStatus') {
            if (existMap(request.url, enableMap)) {
                sendResponse({
                    msg: true
                });
            } else {
                sendResponse({
                    msg: image
                });
            }
        } else if (request.msg == 'titleStatus') {
            if (existMap(request.url, enableMap)) {
                sendResponse({
                    msg: true
                });
            } else {
                sendResponse({
                    msg: title
                });
            }
        } else if (request.msg == 'faviconStatus') {
            if (existMap(request.url, enableMap)) {
                sendResponse({
                    msg: true
                });
            } else {
                sendResponse({
                    msg: favicon
                });
            }
        } else if (request.msg == 'enableStatus') {
            if (existMap(request.url, enableMap)) {
                sendResponse({
                    msg: true
                });
            } else {
                sendResponse({
                    msg: false
                });
            }
        } else {
            sendResponse({
                msg: true
            });
        }
    });

    // 存储变化监听
    chrome.storage.onChanged.addListener(function (changes, areaName) {
        log('storage change: ', changes);
        if (changes.image && changes.image.newValue != undefined) {
            image = changes.image.newValue;
        } else if (changes.title && changes.title.newValue != undefined) {
            title = changes.title.newValue;
        } else if (changes.favicon && changes.favicon.newValue != undefined) {
            favicon = changes.favicon.newValue;
        } else if (changes.notclean && changes.notclean.newValue != undefined) {
            notclean = changes.notclean.newValue;
        } else if (changes.enable && changes.enable.newValue != undefined) {
            enable = changes.enable.newValue;
            enableMap = arrayToMap(enable);
        }
        updateBadge(); // 更新开关图标
    });

    // 拦截url监听
    function filterCallback(details) {
        log('details:', details);
        if (!image &&
            existMap(allTabIdAndUrl[details.tabId], enableMap)
        ) {
            log('白名单: ' + allTabIdAndUrl[details.tabId]);
            return {
                cancel: false
            };
        }
        if (!image &&
            (
                details.type == 'image' ||
                details.type == 'media'
            )
        ) {
            log(details.url, 'cancel:true');
            if (cache.get(details.url) > retry) {
                return {
                    redirectUrl: 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7'
                };
            } else {
                cache.set(details.url);
                return {
                    cancel: true
                };
            }
        }
        if (!image &&
            details.type != 'stylesheet' &&
            details.type != 'script' &&
            details.responseHeaders
        ) {
            for (var i = 0; i < details.responseHeaders.length; i++) {
                var kv = details.responseHeaders[i];
                var name = kv.name.toLowerCase();
                var value = kv.value.toLowerCase();
                if (name == 'content-type' &&
                    (
                        value.indexOf('image/') != -1 ||
                        value.indexOf('video/') != -1 ||
                        value.indexOf('audio/') != -1 ||
                        value == 'application/x-mpegurl' ||
                        value == 'application/mpegurl' ||
                        value == 'application/vnd.apple.mpegurl' ||
                        value == 'application/vnd.apple.mpegurl.audio' ||
                        value == 'application/octet-stream'
                    )
                ) {
                    log(details.url, 'cancel:true');
                    if (cache.get(details.url) > retry) {
                        return {
                            redirectUrl: 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7'
                        };
                    } else {
                        cache.set(details.url);
                        return {
                            cancel: true
                        };
                    }
                }
            }
        }
        log(details.url, 'cancel:false');
        return {
            cancel: false
        };
    }
    chrome.webRequest.onBeforeRequest.addListener(
        filterCallback, {
            urls: ["<all_urls>"]
        },
        ['blocking', 'extraHeaders']
    );
    chrome.webRequest.onHeadersReceived.addListener(
        filterCallback, {
            urls: ["<all_urls>"]
        },
        ['blocking', 'responseHeaders', 'extraHeaders']
    );

    badgeAndUrlUpdate(); // 更新映射关系
}

main(); // main入口