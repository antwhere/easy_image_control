Block images, videos, audio connections, hide title, hide favicon.
Save network bandwidth and data traffic.

✅ open-source: https://bitbucket.org/antwhere/easy_image_control/
----------

Update history:
4.0: Support for pending navigation
3.9: Improve performance
3.8: Block videos and audio connections
3.7: Improve performance
3.6: Improve performance
3.5: Fixed the exclusion list issue in the previous version
3.4: Improve performance
3.3: Add Import/Export Settings feature
3.2: Fine-tune the UI
3.1: Improve performance
3.0: Improve performance
2.9: Add switch symbol
2.8: Improve performance
2.7: Add a reserved cache feature. Modify the UI.
2.6: Update description
2.5: Fixed some bugs
2.4:
    * Rebuild code!
    * New design!
    * Installation package is less than 30KB!
    * Faster! More stable! More savings in computer performance!
2.3: Icon support retina display
2.2: Modify the extension icon
2.1:
    * Add an exclusion list, now you can add websites that need to display pictures to the exclusion list, the operation is simple and easy to use
    * Fixed some bugs

----------

Privacy Policy:
We do not collect any information from the first version until now and forever!
----------

The initial version began in September 2014.
Feedback E-mail: antwhere@antwhere.com
Official Website: www.antwhere.com